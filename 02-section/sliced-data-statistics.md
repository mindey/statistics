#### Segmented means

##### Make sure that variables are of the right types first:


**Only blood glucose:**

```python
In [5]: df[df.variable=='blood:glucose'].value.mean()
Out [5]: 5.04
```

Literally: `(5.34 + 4.74) / 2`.

**Only after 2022-05-05 10:00:**

Change type of date variable to date:

```python
df.date = pandas.to_datetime(df.date)
```

Verify variable types by `df.dtype`. Notice that `date` variable is of type `datetime` now. This allows advanced time-based analysis.

```python
In [6]: df[df.date>='2022-05-05 10:00'].value.mean()
Out [6]: 4.425
```

Literally: `(4.74 + 4.11 ) / 2`.


**Only blood glucose and after 2022-05-05 10:00:**


```python
In [7]: df[(df.variable=='blood:glucose') & (df.date>='2022-05-05 10:00')].value.mean()
Out [7]: 4.74
```

Literally: `4.74`, the only value satisfying the conditions.
