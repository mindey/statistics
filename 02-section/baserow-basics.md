## Small Datasets

Here is the function **`get_table()`** for small datasets not [200 observations](https://baserow.io/docs/installation/configuration#:~:text=BASEROW_ROW_PAGE_SIZE_LIMIT):

```python
# Vanilla Python
import pandas
import requests


def get_table(table_id, get_values=True):
    headers = {"Authorization": f"Token {API_TOKEN}"}

    rows = requests.get(f"{API}api/database/rows/table/{table_id}/", headers=headers)
    fields = requests.get(f"{API}api/database/fields/table/{table_id}/", headers=headers)

    columns = {'field_'+str(field['id']): field['name'] for field in fields.json()}
    df = pandas.DataFrame.from_records(rows.json()['results']).rename(columns=columns)

    if get_values:
        get_val = lambda x: x[0].get('value') if x and isinstance(x, list) and isinstance(x[0], dict) else x
        return df.applymap(get_val)

    return df
```

### Usage of `get_table()`

We may then, use the `get_table()` function, for example:
page size
```python
In [1] API = 'https://db.mindey.com/'
       API_TOKEN = 'C9gZFb15B8KAvWGihyr8YOGm62SSDlTD'
       df = get_table(232)
       df

Out[1]:
   id                   order uid                  Date       Variable    Unit    Value
0   1  1.00000000000000000000   1  2022-04-04T11:00:00Z  blood:glucose  mmol/l  5.34000
1   2  2.00000000000000000000   2  2022-05-20T12:00:00Z  blood:glucose  mmol/l  4.74000
2   3  3.00000000000000000000   3  2022-04-04T11:00:00Z  urine:glucose  mmol/l  4.12000
3   4  4.00000000000000000000   4  2022-05-20T12:00:00Z  urine:glucose  mmol/l  4.11000
```

