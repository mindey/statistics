## Effect size

mean difference, normalized [...]

ref: https://hal.inria.fr/hal-02905210/document
ref: https://en.wikiversity.org/wiki/Cohen%27s_d

## Power

true positives probability [...]

ref: https://www.wikiwand.com/en/Power_of_a_test
