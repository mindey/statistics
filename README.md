# Statistics with Python

This book will introduce doing statistical calculations using Python. It will summarize the example code snippets for doing statistical to produce statistics, mostly with `sympy` and `xarray` packages.

![](cover.png)

<center><a href="./01-section/01-preparation.md"><b>Continue</b> ➙</center>
