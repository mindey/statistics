# Professional Work Process

But first, let's get the work process straight.

We'll manage data using [baserow.io](https://baserow.io/) (get an account or run your own instance), and create an API token.

So, let's say we have a view for data, like this:

<iframe class="airtable-embed" src="https://db.mindey.com/public/grid/HGk13sYwXhKGAI-bY3eK3a_NnAQUu7JwuNCsQtQDXgw" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>

Use the [baserow-client](https://github.com/NiklasRosenstein/python-baserow-client) library to read it:

```python
In [1]:
from fxy.io.baserow import BaserowIO
API = 'https://db.mindey.com/'
API_TOKEN = 'C9gZFb15B8KAvWGihyr8YOGm62SSDlTD'
io = BaserowIO(API, token=API_TOKEN)
df = io.get_table(232)
df
1it [00:00,  4.64it/s]

Out[1]:
   id                   order uid                  date       Variable    Unit    value
0   1  1.00000000000000000000   1  2022-04-04T11:00:00Z  blood:glucose  mmol/l  5.34000
1   2  2.00000000000000000000   2  2022-05-20T12:00:00Z  blood:glucose  mmol/l  4.74000
2   3  3.00000000000000000000   3  2022-04-04T11:00:00Z  urine:glucose  mmol/l  4.12000
3   4  4.00000000000000000000   4  2022-05-20T12:00:00Z  urine:glucose  mmol/l  4.11000

```

Now, we can do the same mean computation, like:

```python
In [2]: df.value = pandas.to_numeric(df.value)
        df.value.mean()
Out[2]: 4.5775
```

**Tip:** To apply "to_numeric" wherever possible, use something like:

```python
df_numeric = df.apply(pandas.to_numeric, errors='coerce').fillna(df)
```

Read more about type conversions [here](https://towardsdatascience.com/how-to-change-datatypes-in-pandas-in-4-minutes-677addf9a409).
