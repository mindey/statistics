# Preparation of computer for statistical work

We'll assume that you're using a [*nix](https://en.wikipedia.org/wiki/Unix-like) operating system, and have [Python](https://www.python.org/) installed.

Create and activate [virtualenv](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/), and get [fxy](https://pypi.org/project/fxy/) package:

`$ python3 -m venv .venv && . .venv/bin/activate`

`$ pip install fxy`

Type `fx -s` or `isympy` command to get started:

`$ fx -s`

`In [1]: █`


_Note:_

- _`fx` starts IPython environment with [mpmath](https://mpmath.org/) pre-imported,_
- _ parameter `-s` additionally imports [sympy](https://www.sympy.org/) with [isympy](https://github.com/sympy/sympy/blob/master/isympy.py) importer.)_
