# Summary

* SEQUENTIAL CHAPTERS
    * [00 Introduction](README.md)
    * [01 Preparation](01-section/01-preparation.md)
    * [02 Statistical Basics](01-section/02-statistical-basics.md)
    * [03 Work Process](01-section/03-work-process.md)
    * [04 Comparing the Means](01-section/04-compare-the-means.md)
* AD-HOC CHAPTERS
    * [BetaPERT Distribution](02-section/beta-pert.md)
